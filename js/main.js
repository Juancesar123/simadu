
var mainApp = angular.module("mainApp", ['ngRoute','datatables','checklist-model',"ngResource"]);
 mainApp.config(function($routeProvider) {
    $routeProvider
      .when('/datasantri', {
        templateUrl:"datasantri",
        controller:"datasantri"
	}).when('/datamatapelajaran', {
    templateUrl:"datamatapelajaran",
    controller:"datamatapelajaran"
}).when('/datakelas', {
    templateUrl:"datakelas",
    controller:"datakelas"
}).when('/datakamar', {
    templateUrl:"datakamar",
    controller:"datakamar"
}).when('/user', {
    templateUrl:"datauser",
    controller:"user"
})
});


 mainApp.factory('socket', ['$rootScope', function($rootScope) {
   var socket = io.connect();

   return {
     on: function(eventName, callback){
       socket.on(eventName, callback);
     },
     emit: function(eventName, data) {
       socket.emit(eventName, data);
     }
   };
 }]);
mainApp.directive('fileModel', ['$parse', function ($parse) {
            return {
               restrict: 'A',
               link: function(scope, element, attrs) {
                  var model = $parse(attrs.fileModel);
                  var modelSetter = model.assign;

                  element.bind('change', function(){
                     scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                     });
                  });
               }
            };
         }]);
		mainApp.service('fileUpload', ['$http', function ($http,$scope) {
    this.uploadFileToUrl = function(nama,tanggal,tempat,alamat,jenis,notlp,gambar,uploadUrl){
        var fd = new FormData();
        fd.append('nama', nama);
        fd.append('tanggal', tanggal);
        fd.append('tempat', tempat);
        fd.append('alamat', alamat);
        fd.append('jeniskelamin', jenis);
        fd.append('notlp',notlp);
        fd.append('gambar', gambar);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
			alert("data sukses diupload");
			$http.get("lihat_datasantri").success(function(data){
		datasantri = data;
			});

        })
        .error(function(){
				alert("data gagal di input");
        });

    }

}]);
mainApp.service('fileEdit', ['$http', function ($http,$scope) {
    this.uploadFileToUrl = function(id,nama,tanggal,tempat,alamat,jenis,notlp,gambar,uploadUrl){
        var fd = new FormData();
        fd.append('id', id);
        fd.append('nama', nama);
        fd.append('tanggal', tanggal);
        fd.append('tempat', tempat);
        fd.append('alamat', alamat);
        fd.append('jeniskelamin', jenis);
        fd.append('notlp',notlp);
        fd.append('gambar', gambar);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
      alert("data sukses diupload");
      $http.get("lihat_datasantri").success(function(data){
    datasantri = data;
      });

        })
        .error(function(){
        alert("data gagal di input");
        });

    }

}]);
mainApp.service('fileuploadkamar', ['$http', function ($http,$scope) {
    this.uploadFileToUrl = function(nomor,fasilitas,gambar,uploadUrl){
        var fd = new FormData();
        fd.append('nomor', nomor);
        fd.append('fasilitas', fasilitas);
        fd.append('gambar', gambar);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
      alert("data sukses diupload");
      $http.get("lihat_datakamar").success(function(data){
    datakamar = data;
      });

        })
        .error(function(){
        alert("data gagal di input");
        });

    }

}]);
mainApp.service('fileuploadkamarubah', ['$http', function ($http,$scope) {
    this.uploadFileToUrl = function(id,gambar1,nomor,fasilitas,gambar,uploadUrl){
        var fd = new FormData();
        fd.append('id', id);
        fd.append('gambar1', gambar1);
        fd.append('nomor', nomor);
        fd.append('fasilitas', fasilitas);
        fd.append('gambar', gambar);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
      alert("data sukses diupload");
      $http.get("lihat_datakamar").success(function(data){
    datakamar = data;
      });

        })
        .error(function(){
        alert("data gagal di input");
        });

    }

}]);
mainApp.controller("datasantri",function($scope,DTOptionsBuilder,DTColumnBuilder,$http,fileUpload){
    $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withDisplayLength(5)
    .withOption('bLengthChange', false)
    .withOption('autoWidth', false)
    .withOption('scrollX', false);
    $scope.getdata = function(){
        $http.get("lihat_datasantri").success(function(data){
        $scope.datasantri= data;
      });
    }
$scope.getdata();
$scope.simpan=function(){
    var nama = $scope.nama;
    var tanggal = $scope.tanggal;
    var alamat = $scope.alamat;
    var jeniskelamin = $scope.jenis;
    var notlp = $scope.notlp;
    var gambar = $scope.gambar;
    var tempat = $scope.tempat;
    var datasantri = $scope.datasantri;
    var uploadUrl = "simpan_datasantri";
   fileUpload.uploadFileToUrl(nama,tanggal,tempat,alamat,jeniskelamin,notlp,gambar,uploadUrl);
}
$scope.edit=function(item){
  $scope.nama = item.nama;
  $scope.tanggaal = item.tanggal;
  $scope.alamat = item.alamat;
  $scope.jenis = item.jenis;
  $scope.notlp = item.notlp;
  $scope.gambar = item.gambar
  $scope.tempat = item.tempat;
  $scope.id = item.id;
}
$scope.actionedit=function(){
 var nama = $scope.nama;
    var tanggal = $scope.tanggal;
    var alamat = $scope.alamat;
    var jeniskelamin = $scope.jenis;
    var notlp = $scope.notlp;
    var gambar = $scope.gambar;
    var tempat = $scope.tempat;
    var id = $scope.id;
    var datasantri = $scope.datasantri
    var uploadUrl = "ubah_datasantri";
    if(gambar==undefined){
      $http.post("ubah_datasantri_noimage",{
        nama:nama,
        id:id,
        tanggal:tanggal,
        alamat:alamat,
        jeniskelamin:jeniskelamin,
        notlp:notlp,
        tempat:tempat,
        alamat:alamat}).success(function(){
        $scope.getdata();
        alert("data sukses di ubah")
      })
    }else{
   fileEdit.uploadFileToUrl(id,nama,tanggal,tempat,alamat,jeniskelamin,notlp,gambar,uploadUrl); 
}
}
$scope.user={
  hapussantri:[]
}
$scope.hapus = function(){
  var id = $scope.user;
  $http.post("hapus_datasantri",{id:id}).success(function(){
    $scope.getdata();
    alert("data sukses di hapus");
  })
}
})
mainApp.controller("datamatapelajaran",function($scope,DTOptionsBuilder,DTColumnBuilder,$http){
    $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withDisplayLength(5)
    .withOption('bLengthChange', false)
    .withOption('autoWidth', false)
    .withOption('scrollX', false);
    $scope.getdata = function(){
        $http.get("ambil_datamatpel").success(function(data){
        $scope.datamatpel= data;
      });
    }
$scope.getdata();
      $scope.simpan = function(){
      var matpel = $scope.matpel;
      $http.post("simpan_datamatpel",{matpel:matpel}).success(function(){
      alert("data sukses di input");
      $scope.getdata();
    })
  }
  $scope.edit=function(item){
    $scope.matpel = item.nama_matpel;
    $scope.id = item.id_matpel;
  }
  $scope.actionedit = function(){
    var matpel = $scope.matpel;
    var id = $scope.id;
      $http.post("ubah_datamatpel",{matpel:matpel,id:id}).success(function(){
      alert("data sukses di ubah");
      $scope.getdata();
    })
  }
  $scope.user={
    hapusdatamatpel:[]
  }
  $scope.hapus = function(){
    var id = $scope.user;
    $http.post("hapus_datamatpel",{id:id}).success(function(){
      alert("data sukses di hapus");
      $scope.getdata();
    })
  }
})
mainApp.controller("datakelas",function($scope,DTOptionsBuilder,DTColumnBuilder,$http){
    $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withDisplayLength(5)
    .withOption('bLengthChange', false)
    .withOption('autoWidth', false)
    .withOption('scrollX', false);
    $scope.getdata = function(){
        $http.get("ambil_datakelas").success(function(data){
        $scope.datakelas= data;
      });
    }
$scope.getdata();
$scope.simpan = function(){
  var kelas = $scope.kelas;
  $http.post("tambah_datakelas",{kelas:kelas}).success(function(){
    alert("data sukses di input");
    $scope.getdata();
  })
}
$scope.edit = function(item){
  $scope.kelas = item.kelas;
  $scope.id = item.id_kelas;
}
$scope.actionedit = function(){
   var kelas = $scope.kelas;
   var id = $scope.id;
  $http.post("ubah_datakelas",{kelas:kelas,id:id}).success(function(){
    alert("data sukses di ubah");
    $scope.getdata();
  })
}
$scope.user={
  hapusdatakelas:[]
}
$scope.hapus= function(){
  var id = $scope.user;
  $http.post("hapus_datakelas",{id:id}).success(function(){
    alert("data sukses di hapus");
    $scope.getdata();
  })
}
})
mainApp.controller("datakamar",function($scope,DTOptionsBuilder,DTColumnBuilder,$http,fileuploadkamar,fileuploadkamarubah){
   $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withDisplayLength(5)
    .withOption('bLengthChange', false)
    .withOption('autoWidth', false)
    .withOption('scrollX', false);
    $scope.getdata = function(){
        $http.get("lihat_datakamar").success(function(data){
        $scope.datakamar= data;
      });
    }
$scope.getdata();

$scope.simpan = function(){
  var nomor = $scope.nomor;
  var fasilitas = $scope.fasilitas;
  var gambar = $scope.gambar;
  var datakamar = $scope.datakamar;
  var uploadUrl = "tambah_datakamar";
  fileuploadkamar.uploadFileToUrl(nomor,fasilitas,gambar,uploadUrl); 
}
$scope.user={
  hapusdatakamar:[]
}
$scope.edit=function(item){
  $scope.nomor = item.nomor;
  $scope.fasilitas = item.fasilitas;
  $scope.gambar1 = item.gambar
  $scope.id = item.id_kamar;
}
$scope.actionedit=function(){
  var nomor = $scope.nomor;
  var fasilitas = $scope.fasilitas;
  var gambar = $scope.gambar;
   var gambar1 = $scope.gambar1;
  var datakamar = $scope.datakamar;
  var uploadUrl = "ubah_datakamar";
  var id = $scope.id
  if(gambar==undefined){
    $http.post("ubah_datakamar_noimage",{id:id,nomor:nomor,fasilitas:fasilitas}).success(function(){
      alert("data sukses di ubah");
      $scope.getdata()
    })
  }else{
    fileuploadkamarubah.uploadFileToUrl(id,gambar1,nomor,fasilitas,gambar,uploadUrl); 
  }
}
$scope.hapus = function(){
  var id = $scope.user;
  $http.post("hapus_datakamar",{id:id}).success(function(){
    alert("data sukses di hapus")
    $scope.getdata();
  })
};
})
mainApp.controller("user",function($scope,DTOptionsBuilder,DTColumnBuilder,$http){
    $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withDisplayLength(5)
    .withOption('bLengthChange', false)
    .withOption('autoWidth', false)
    .withOption('scrollX', false);
    $scope.getdata = function(){
        $http.get("ambil_datauser").success(function(data){
        $scope.datauser= data;
      });
    }
$scope.getdata();
$scope.simpan = function(){
  var firstname = $scope.firstname;
  var lastname = $scope.lastname;
  var password = $scope.password;
  var level = $scope.level;
  var email = $scope.email;
  $http.post("tambah_datauser",{firstname:firstname,lastname:lastname,password:password,email:email,level:level}).success(function(){
    alert("data sukses di simpan");
    $scope.getdata();
  })
}
$scope.edit = function(item){
  $scope.firstname = item.first_name;
  $scope.lastname = item.last_name;
  $scope.level = item.level;
  $scope.email = item.email;
  $scope.id = item.id;
}
$scope.actionedit = function(){
  var firstname = $scope.firstname;
  var lastname = $scope.lastname;
  var password = $scope.password;
  var level = $scope.level;
  var email = $scope.email;
  var id = $scope.id
  $http.post("ubah_datauser",{id:id,firstname:firstname,lastname:lastname,password:password,email:email,level:level}).success(function(){
    alert("data sukses di ubah");
    $scope.getdata();
  })
}
$scope.user={
  hapusdatauser:[]
}
$scope.hapus = function(){
  var id = $scope.user;
  $http.post("hapus_datauser",{id:id}).success(function(){
    alert("data sukses di hapus");
    $scope.getdata();
  })
}
})