<?php
class Datauser extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('model_datauser');
	}
	public function index(){
		$this->load->view("datauser1");
	}
	public function ambil_datauser(){
		$data = $this->model_datauser->ambil_datauser();
		echo json_encode($data);
	}
	public function tambah_datauser(){

		$data = (array)json_decode(file_get_contents('php://input'));
		$username = $data['firstname'];
		$password = $data['password'];
		$email = $data['email'];
		$additional_data = array(
								'first_name' => $data['firstname'],
								'last_name' => $data['lastname'],
								);
		$group = array($data['level']); // Sets user to admin.
		$this->ion_auth->register($username, $password, $email, $additional_data, $group);
	
	}
	public function ubah_datauser(){
	
		$data = (array)json_decode(file_get_contents('php://input'));
		$id = $data['id'];
		$data = array(
					'first_name' => $data['firstname'],
					'last_name' => $data['lastname'],
					'email' => $data['email'],
					'level' => $data['level']
					 );
		$this->ion_auth->update($id, $data);
	
	}
	public function hapus_datauser(){
		$data = (array)json_decode(file_get_contents('php://input'));
		for($i = 0; $i<count($data['id']);$i++){
		$this->ion_auth->delete_user($data['id']->hapusdatauser[$i]);
		}
	}
}