<?php
Class Datamatapelajaran extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('model_datamatapel');
	}
	public function index(){
		$this->load->view("matapelajaran");
	}
	public function ambil_datamatpel(){
		$data = $this->model_datamatapel->ambil_datamatpel();
		echo json_encode($data);

	}
	public function tambah_datamatpel(){
		$data = (array)json_decode(file_get_contents('php://input'));
		$val = array(
			"nama_matpel" => $data['matpel']
			);
		$this->model_datamatapel->tambah_datamatpel($val);
	}
	public function ubah_datamatpel(){
		$data = (array)json_decode(file_get_contents('php://input'));
		$val = array(
			"nama_matpel" => $data['matpel']
		);
		$id = $data["id"];
		$this->model_datamatapel->ubah_datamatpel($val,$id);
	}
	public function hapus_datamatpel(){
		$data = (array)json_decode(file_get_contents('php://input'));
		for($i=0;$i<count($data["id"]);$i++){
			$id = $data["id"]->hapusdatamatpel[$i];
			$this->model_datamatapel->hapus_datamatpel($id);

		}
	}
}