<?php
class Datakelas extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('model_datakelas');
	}
	public function index(){
		$this->load->view("datakelas");
	}
	public function ambil_datakelas(){
		$data = $this->model_datakelas->ambil_datakelas();
		echo json_encode($data);
	}
	public function tambah_datakelas(){
		$data = (array)json_decode(file_get_contents('php://input'));
		$val = array(
			"kelas" => $data['kelas']
			);
		$this->model_datakelas->tambah_datakelas($val);
	}
	public function ubah_datakelas(){
		$data = (array)json_decode(file_get_contents('php://input'));
		$val = array(
			"kelas" => $data['kelas']
			);
	$id = $data['id'];
	$this->model_datakelas->ubah_datakelas($val,$id);
	}
	public function hapus_datakelas(){
		$data = (array)json_decode(file_get_contents('php://input'));
		for($i=0;$i<count($data['id']);$i++){
		$id = $data['id']->hapusdatakelas[$i];
		$this->model_datakelas->hapus_datakelas($id);
		
		}
	}
}