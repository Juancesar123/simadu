<?php
class Datakamar extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('model_datakamar');
	}
	public function index(){
		$this->load->view("datakamar1");
	}
	public function tambah_datakamar(){
				$time = date('YmdHis');
				$config['upload_path']          = './gambarkamar/';
                $config['allowed_types']        = 'gif|jpg|png';
				$config['file_name']        	= "F_".$time;
                $this->load->library('upload', $config);
                $extendtion = explode(".", $_FILES['gambar']['name']);
					$foto_path = "gambarkamar/".$config['file_name'].".".$extendtion[count($extendtion)-1];
               
                if ( ! $this->upload->do_upload('gambar'))
                {
                        $error = array('error' => $this->upload->display_errors());

                        $this->load->view('upload_form', $error);
                }
                else
                {
                 $val = array(
                 	"nomor_kamar" => $this->input->post("nomor"),
                 	"fasilitas" =>$this->input->post("fasilitas"),
                 	"gambar" =>$foto_path,
                 	);
                 $this->model_datakamar->tambah_datakamar($val);
                }   
	}
	public function ubah_datakamar(){
		$time = date('YmdHis');
				$config['upload_path']          = './gambarkamar/';
                $config['allowed_types']        = 'gif|jpg|png';
				$config['file_name']        	= "F_".$time;
                $this->load->library('upload', $config);
                unlink($this->input->post("gambar1"));
                $extendtion = explode(".", $_FILES['gambar']['name']);
					$foto_path = "gambarkamar/".$config['file_name'].".".$extendtion[count($extendtion)-1];
               
                if ( ! $this->upload->do_upload('gambar'))
                {
                        $error = array('error' => $this->upload->display_errors());

                        $this->load->view('upload_form', $error);
                }
                else
                {
                 $val = array(
                 	"nomor_kamar" => $this->input->post("nomor"),
                 	"fasilitas" =>$this->input->post("fasilitas"),
                 	"gambar" =>$foto_path,
                 	);
                 $id = $this->input->post("id");
                 $this->model_datakamar->ubah_datakamar($val,$id);
                }
	}
	public function ubahdatakamarnoimage(){

	}
	public function hapus_datakamar(){
	
		$data = (array)json_decode(file_get_contents('php://input'));
		print_r($data['id']->hapusdatakamar[0]->id_kamar);
		for($i = 0; $i < count($data['id']->hapusdatakamar);$i++){
			unlink($data['id']->hapusdatakamar[$i]->gambar);
			$id = $data['id']->hapusdatakamar[$i]->id_kamar;
			$this->model_datakamar->hapusdatakamar($id);
		}
	}
	public function ambil_datakamar(){
		$data = $this->model_datakamar->ambil_datakamar();
		echo json_encode($data);
	}
}