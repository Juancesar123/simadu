<?php
class Datasantri extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('model_datasantri');
		$this->load->helper('date');
	}
	public function index(){
		$this->load->view("datasantri");
	}
	public function lihatdatasantri(){
		$data = $this->model_datasantri->lihatdatasantri();
		echo json_encode($data);
	}
	public function ubahdatasantri(){
		$data = (array)json_decode(file_get_contents('php://input'));
	}
	public function ubahdatasantrinoimage(){
		$data = (array)json_decode(file_get_contents('php://input'));
		$val = array(
			'nama' => $data['nama'],
			'alamat' => $data['alamat'],
			'notlp' => $data['notlp'],
			'tempat' => $data['tempat'],
			'tanggal' => $data['tanggal'],
			'jenis' => $data['jeniskelamin']
			);
		$id = $data['id'];
		$this->model_datasantri->ubahdatasantrinoimage($id,$val);
	}
	public function hapusdatasantri(){
		$data = (array)json_decode(file_get_contents('php://input'));
		for($i = 0; $i < count($data['id']);$i++){
			$id = $data['id']->hapussantri[$i];
			$this->model_datasantri->hapusdatasantri($id);
		}
	}

	public function simpandatasantri(){
				$time = date('YmdHis');
				$config['upload_path']          = './gambarsantri/';
                $config['allowed_types']        = 'gif|jpg|png';
				$config['file_name']        	= "F_".$time;
                $this->load->library('upload', $config);
                $extendtion = explode(".", $_FILES['gambar']['name']);
					$foto_path = "gambarsantri/".$config['file_name'].".".$extendtion[count($extendtion)-1];
                if ( ! $this->upload->do_upload('gambar'))
                {
                	return flase;
                }
                else
                {
        			$val = array(
        					'nama' => $this->input->post('nama'),
							'alamat' => $this->input->post('alamat'),
							'notlp' => $this->input->post('notlp'),
							'tempat' => $this->input->post('tempat'),
							'tanggal' => $this->input->post('tanggal'),
							'jenis' => $this->input->post('jeniskelamin'),
							'foto' => $foto_path
							);        
               		$this->model_datasantri->simpandatasantri($val);
                }     
	      }
     }