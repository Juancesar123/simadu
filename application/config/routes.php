<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Auth/login';
$route['logout'] = 'Auth/logout';
$route['datasantri'] = 'Datasantri';
$route['lihat_datasantri'] = 'Datasantri/lihatdatasantri';
$route['ubah_datasantri'] = 'Datasantri/ubahdatasantri';
$route['ubah_datasantri_noimage'] = 'Datasantri/ubahdatasantrinoimage';
$route['hapus_datasantri'] = 'Datasantri/hapusdatasantri';
$route['simpan_datasantri'] = 'Datasantri/simpandatasantri';
$route['datamatapelajaran'] = "Datamatapelajaran";
$route['ambil_datamatpel'] = "datamatapelajaran/ambil_datamatpel";
$route['ubah_datamatpel'] = "datamatapelajaran/ubah_datamatpel";
$route['hapus_datamatpel']= "datamatapelajaran/hapus_datamatpel";
$route['simpan_datamatpel'] = "datamatapelajaran/tambah_datamatpel";
$route['ubah_datamatpel'] = "datamatapelajaran/ubah_datamatpel";
$route['datakelas'] = "Datakelas";
$route["ambil_datakelas"] = "Datakelas/ambil_datakelas";
$route["tambah_datakelas"] = "Datakelas/tambah_datakelas";
$route["ubah_datakelas"] = "Datakelas/ubah_datakelas";
$route["hapus_datakelas"] = "Datakelas/hapus_datakelas";
$route["lihat_datakamar"] = "Datakamar/ambil_datakamar";
$route["ubah_datakamar"] = "Datakamar/ubah_datakamar";
$route["ubah_datakamar_noimage"] = "Datakamar/ubahdatakamarnoimage";
$route["hapus_datakamar"] = "Datakamar/hapus_datakamar";
$route["tambah_datakamar"] = "Datakamar/tambah_datakamar";
$route['datakamar'] = "Datakamar";
$route['datauser'] = "Datauser";
$route['ambil_datauser'] = "Datauser/ambil_datauser";
$route['tambah_datauser'] = "Datauser/tambah_datauser";
$route['ubah_datauser'] = "Datauser/ubah_datauser";
$route['hapus_datauser'] = "Datauser/hapus_datauser";
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
