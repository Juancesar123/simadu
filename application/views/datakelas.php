<html>
<head>
</head>
<body>
 <section class="content-header">
      <h1>
       Data Kelas 
        <small>Data Kelas</small>
      </h1>
      <ol class="breadcrumb">
        <li><i class = "fa fa-database"></i> Master Data</li>
        <li class="active"><i class = "fa fa-users"></i> Data Kelas</li>
      </ol>
    </section>
    <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Input Kelas</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
        <label>Kelas</label>
        <input type="text" class="form-control" ng-model="kelas">
      	</div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" ng-click="simpan()"><i class="fa fa-send"></i> Kirim</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ubah Kelas</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
        <label>Kelas</label>
        <input type="text" class="form-control" ng-model="kelas">
        </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-success" ng-click="actionedit()"><i class="fa fa-send"></i> Kirim</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    <div class="box box-primary">
<div class="box-header">
<button class="btn btn-primary" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Kelas</button>
<button class="btn btn-danger" ng-click="hapus()"><i class="fa fa-trash"></i> Hapus Data Kelas</button>
</div>
<div class="box-body">
<table datatable="ng" dt-columns="dtColumns" dt-options="dtOptions"class="table table-bordered table-striped">
<thead>
<th><input type="checkbox" ng-click="checkall()"></th>
<th>Kelas</th>
<th>Action</th>
</thead>
<tbody>
<tr ng-repeat="item in datakelas">
 <td><input type="checkbox"  checklist-model="user.hapusdatakelas" checklist-value="item.id_kelas" role></td>
<td>{{item.kelas}}</td>
<td><button class="btn btn-success" ng-click="edit(item)" data-toggle="modal" data-target="#myModal1"><i class="fa fa-edit"></i> Edit</button></td>
</tr>
</tbody>
</table>
</div>
</div>
</body>
</html>
