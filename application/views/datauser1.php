<div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
             <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Input user</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
        <div class="form-line">
        <label>Nama Depan</label>
        <input type="text" class="form-control" ng-model="firstname">
        </div>
      	</div>
        <div class="form-group">
        <div class="form-line">
        <label>Nama Belakang</label>
        <input type="text" class="form-control" ng-model="lastname">
        </div>
        </div>
        <div class="form-group">
        <div class="form-line">
        <label>Email</label>
        <input type="text" class="form-control" ng-model="email">
        </div>
        </div>
        <div class="form-group">
        <div class="form-line">
        <label>password</label>
        <input type="text" class="form-control" ng-model="password">
        </div>
        </div>
        <div class="form-group">
        <div class="form-line">
        <label>Level</label>
        <select class="form-control" ng-model="level">
        <option value="1">Administrator</option>
        <option value="2">Murid</option>
        </select>
        </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" ng-click="simpan()"><i class="fa fa-send"></i> Kirim</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ubah User</h4>
      </div>
      <div class="modal-body">
         <div class="form-group">
        <div class="form-line">
        <label>Nama Depan</label>
        <input type="text" class="form-control" ng-model="firstname">
        </div>
        </div>
        <div class="form-group">

        <div class="form-line">  
        <label>Nama Belakang</label>
        <input type="text" class="form-control" ng-model="lastname">
        </div>
        </div>
        <div class="form-group">
        <div class="form-line">

        <label>Email</label>
        <input type="text" class="form-control" ng-model="email">
        </div>
        </div>
        <div class="form-group">
        <div class="form-line">

        <label>Level</label>
        <select class="form-control" ng-model="level">
        <option value="1">Administrator</option>
        <option value="2">Murid</option>
        </select>
        </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-success" ng-click="actionedit()"><i class="fa fa-send"></i> Kirim</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
 <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <button class="btn btn-primary" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Data User</button>
                            <button class="btn btn-danger" ng-click="hapus()"><i class="fa fa-trash"></i> Hapus Data User</button>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
<div class="body">
<table datatable="ng" dt-columns="dtColumns" dt-options="dtOptions"class="table table-bordered table-striped">
<thead>
<th><input type="checkbox" ng-click="checkall()"></th>
<th>Nama Depan</th>
<th>Nama Belakang</th>
<th>Email</th>
<th>Level</th>
<th>Action</th>
</thead>
<tbody>
<tr ng-repeat="item in datauser">
 <td><input type="checkbox"  checklist-model="user.hapusdatauser" checklist-value="item.id" id="{{item.first_name}}" class="filled-in" role><label for="{{item.first_name}}"></label></td>
<td>{{item.first_name}}</td>
<td>{{item.last_name}}</td>
<td>{{item.email}}</td>
<td>{{item.level}}</td>
<td><button class="btn btn-success" ng-click="edit(item)" data-toggle="modal" data-target="#myModal1"><i class="fa fa-edit"></i> Edit</button></td>
</tr>
</tbody>
</table>
</div>
 </div>
 </div>
  </div>