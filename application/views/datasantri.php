<html>
<head>
</head>
<body>
 <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
    <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Input Santri</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
        <div class="form-line">
        <label>Nama</label>
        <input type="text" class="form-control" ng-model="nama">
        </div>
      	</div>
      	 <div class="form-group">
         <div class="form-line">
        <label>Alamat</label>
		<textarea ng-model="alamat" class="form-control"></textarea>
    </div>
      	</div>
      	<div class="form-group">
        <div class="form-line">
        <label>Jenis Kelamin</label>
        <select class="form-control" ng-model="jenis">
        	<option value="laki-laki">Laki-Laki</option>
        	<option value="perempuan">Perempuan</option>
        </select>
        </div>
      	</div>
      	 <div class="form-group">
         <div class="form-line">
        <label>Nomor telpon</label>
		<input type="tlp" class="form-control" ng-model="notlp">
    </div>
      	</div>
      	<div class="form-group">
        <div class="form-line">
        <label>Tempat Tanggal lahir</label>
        <div class="form-line">
		<input type="text" class="form-control" ng-model="tempat">
    </div>
    <input type="date" class="form-control" ng-model="tanggal">
    </div>
      	</div>
      	 <div class="form-group">
         <div class="form-line">
        <label>Gambar</label>
		<input type="file" class="form-control" file-model="gambar">
    </div>
      	</div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" ng-click="simpan()"><i class="fa fa-send"></i> Kirim</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ubah Santri</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
        <div class="form-line">
        <label>Nama</label>
        <input type="text" class="form-control" ng-model="nama">
        </div>
        </div>
         <div class="form-group">
         <div class="form-line">
        <label>Alamat</label>
    <textarea ng-model="alamat" class="form-control"></textarea>
    </div>
        </div>
        <div class="form-group">
        <div class="form-line">
        <label>Jenis Kelamin</label>
        <select class="form-control" ng-model="jenis">
          <option value="laki-laki">Laki-Laki</option>
          <option value="perempuan">Perempuan</option>
        </select>
        </div>
        </div>
         <div class="form-group">
         <div class="form-line">
        <label>Nomor telpon</label>
    <input type="tlp" class="form-control" ng-model="notlp">
    </div>
        </div>
        <div class="form-group">
        <div class="form-line">
        <label>Tempat Tanggal lahir</label>
        <div class="form-line">
    <input type="text" class="form-control" ng-model="tempat">
    </div>
    <input type="date" class="form-control" ng-model="tanggal">
    </div>
        </div>
         <div class="form-group">
         <div class="form-line">
        <label>Gambar</label>
    <input type="file" class="form-control" file-model="gambar">
    </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-success" ng-click="actionedit()"><i class="fa fa-send"></i> Kirim</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
     <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                          
                         <button class="btn btn-primary" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Santri</button>
                          <button class="btn btn-danger" ng-click="hapus()"><i class="fa fa-trash"></i> Hapus Data Santri</button>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

<div class="body">
<table datatable="ng" dt-columns="dtColumns" dt-options="dtOptions"class="table table-bordered table-striped">
<thead>
<th><input type="checkbox" ng-click="checkall()"></th>
<th>Nama Santri</th>
<th>Alamat</th>
<th>Nomor Telpon</th>
<th>Tempat Lahir</th>
<th>Tanggal Lahir</th>
<th>Jenis Kelamin</th>
<th>Foto</th>
<th>Tanggal Daftar</th>
<th>Status</th>
<th>Action</th>
</thead>
<tbody>
<tr ng-repeat="item in datasantri">
 <td><input type="checkbox"  checklist-model="user.hapussantri" checklist-value="item.id" id="{{item.nama}}" class="filled-in" role><label for="{{item.nama}}"></label></td>
<td>{{item.nama}}</td>
<td>{{item.alamat}}</td>
<td>{{item.notlp}}</td>
<td>{{item.tempat}}</td>
<td>{{item.tanggal}}</td>
<td>{{item.jenis}}</td>
<td><img ng-src="{{item.foto}}" width="100" height="100"></td>
<td>{{item.tgl_daftar}}</td>
<td>{{item.status}}</td>
<td><button class="btn btn-success" ng-click="edit(item)" data-toggle="modal" data-target="#myModal1"><i class="fa fa-edit"></i> Edit</button></td>
</tr>
</tbody>
</table>
</div>
 </div>
                </div>
            </div>
</body>
</html>
