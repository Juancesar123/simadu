 <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
 <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Input Kamar</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
        <div class="form-line">
        <label>Nomor Kamar</label>
        <input type="text" class="form-control" ng-model="nomor">
        </div>
        </div>
        <div class="form-group">
        <div class="form-line">
        <label>Fasilitas</label>
        <textarea class="form-control" ng-model="fasilitas"></textarea>
        </div>
        </div>
        <div class="form-group">
        <div class="form-line">
        <label>Gambar Kamar</label>
        <input type="file" class="form-control" file-model="gambar">
        </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" ng-click="simpan()"><i class="fa fa-send"></i> Kirim</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ubah Data Kamar</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
        <div class="form-line">
        <label>Nomor Kamar</label>
        <input type="text" class="form-control" ng-model="nomor" placeholder="masukkan nomor kamar">
        </div>
        </div>           
        <div class="form-group">
        <div class="form-line">
        <label>Fasilitas</label>
        <textarea class="form-control" ng-model="fasilitas"></textarea>
        </div>
        </div>
        <div class="form-group">
        <div class="form-line">
        <label>Gambar Kamar</label>
        <input type="file" class="form-control" file-model="gambar">
        </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-success" ng-click="actionedit()"><i class="fa fa-send"></i> Kirim</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
            <!-- Widgets -->
              <!-- CPU Usage -->
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <button class="btn btn-primary" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Kamar</button>
                            <button class="btn btn-danger" ng-click="hapus()"><i class="fa fa-trash"></i> Hapus Data Kamar</button>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <table datatable="ng" dt-columns="dtColumns" dt-options="dtOptions"class="table table-bordered table-striped">

                        <thead>
                        <th><input type="checkbox" ng-click="checkall()"></th>
                        <th>Nomor Kamar</th>
                        <th>Fasilitas</th>
                        <th>Gambar</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        <tr ng-repeat="item in datakamar">
                        <td><input type="checkbox" id="{{item.nomor_kamar}}" class="filled-in" checklist-model="user.hapusdatakamar" checklist-value="item" role><label for="{{item.nomor_kamar}}"></label></td>
                        <td>{{item.nomor_kamar}}</td>
                        <td>{{item.fasilitas}}</td>
                        <td><img ng-src="{{item.gambar}}" width="100" height="100"></td>
                        <td><button class="btn btn-success" ng-click="edit(item)" data-toggle="modal" data-target="#myModal1"><i class="fa fa-edit"></i> Edit</button></td>
                        </tr>
                        </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>