<?php
class Model_datamatapel extends CI_Model{
	public function tambah_datamatpel($val){
		$this->db->insert("data_matapelajaran",$val);
	}
	public function hapus_datamatpel($id){
		$this->db->where("id_matpel",$id);
		$this->db->delete("data_matapelajaran");
	}
	public function ubah_datamatpel($val,$id){
		$this->db->where("id_matpel",$id);
		$this->db->update("data_matapelajaran",$val);
	}
	public function ambil_datamatpel(){
		$sql = "select * from data_matapelajaran";
		return $this->db->query($sql)->result();
	}
}