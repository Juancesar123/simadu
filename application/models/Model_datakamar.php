<?php
class Model_datakamar extends CI_Model{
	public function ambil_datakamar(){
		$sql = "select * from kamar";
		return $this->db->query($sql)->result();
	}
	public function tambah_datakamar($val){
		$this->db->insert("kamar",$val);
	}
	public function ubah_datakamar($val,$id){
		$this->db->where("id_kamar",$id);
		$this->db->update("kamar",$val);
	}
	public function hapusdatakamar($id){
		$this->db->where("id_kamar",$id);
		$this->db->delete("kamar");
		
	}
}