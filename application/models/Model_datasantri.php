<?php
class Model_datasantri extends CI_Model{
	public function lihatdatasantri(){
		$sql = "select * from data_santri";
		return $this->db->query($sql)->result();
	}
	public function simpandatasantri($val){
		$this->db->insert('data_santri',$val);
	}
	public function ubahdatasantri($val,$id){
		$this->db->where("id");
		$this->db->update("data_santri",$val);
	}
	public function hapusdatasantri($id){
		$this->db->where('id',$id);
		$this->db->delete('data_santri');
	}
	public function ubahdatasantrinoimage($id,$val){
		$this->db->where("id",$id);
		$this->db->update("data_santri",$val);
	}
}