<?php
class Model_datakelas extends CI_Model{
	public function ambil_datakelas(){
		$sql = "select * from datakelas";
		return $this->db->query($sql)->result();
	}
	public function tambah_datakelas($val){
		$this->db->insert("datakelas",$val);
	}
	public function ubah_datakelas($val,$id){
		$this->db->where("id_kelas",$id);
		$this->db->update("datakelas",$val);
	}
	public function hapus_datakelas($id){
		$this->db->where("id_kelas",$id);
		$this->db->delete("datakelas");
	}
}